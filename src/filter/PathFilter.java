package filter;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


public class PathFilter implements Filter {

    public PathFilter() {
    }

	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		HttpServletRequest req = (HttpServletRequest) request;
		HttpServletResponse res = (HttpServletResponse) response;
		HttpSession session = req.getSession();
		String defaultPage=req.getContextPath()+"/";
		String loginURI = req.getContextPath() + "/login.jsp";
		String forbiddenURI = req.getContextPath() + "/forbidden.html";
		String resources=req.getContextPath() + "/resources";
		String httpAppServlet=req.getContextPath()+"/http/login";
		String httpFileServlet=req.getContextPath()+"/http/file";
		String homeServlet=req.getContextPath()+"/http/home";
		String favicon="/favicon.ico";
		System.out.println("=================================================");
		System.out.println(req.getRequestURI());
		if(req.getRequestURI().startsWith(resources) || req.getRequestURI().equals(forbiddenURI)) {
			chain.doFilter(req, res);
		}
		else if(req.getRequestURI().equals(homeServlet) && session.getAttribute("username") == null) {
			res.sendRedirect(loginURI);
		}
		else if(req.getRequestURI().equals(httpFileServlet) && session.getAttribute("username") == null) {
			res.sendRedirect(loginURI);
		}
		else if(req.getRequestURI().equals(httpAppServlet) && session.getAttribute("username") != null) {
			req.getRequestDispatcher("/http/home").forward(req, res);
		}
		else if(req.getRequestURI().equals(loginURI)  || req.getRequestURI().equals(favicon) 
			|| req.getRequestURI().equals(defaultPage) || req.getRequestURI().equals(httpFileServlet)
			|| req.getRequestURI().equals(homeServlet) || req.getRequestURI().equals(httpAppServlet)) {
			chain.doFilter(req, res);
		}
		else {
			res.sendRedirect(loginURI);
		}
	}


}
