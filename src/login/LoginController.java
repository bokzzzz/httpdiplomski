package login;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

import DAO.UserDAO;

public class LoginController {
	
	public static boolean isCredentialsOK(String username,String password)  {
		boolean isOK=false;
		UserDAO userDAO=new UserDAO();
		
		try {
			if(userDAO.getUserByUsernameAndPassword(username, getHash(password)) != null) {
				isOK=true;
			}
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		
		return isOK;
	}
	private static String getHash(String password) throws NoSuchAlgorithmException {
		MessageDigest digest = MessageDigest.getInstance("SHA3-256");
		byte[] hashbytes = digest.digest(
		  password.getBytes());
		return Base64.getEncoder().encodeToString(hashbytes);
	}
}
