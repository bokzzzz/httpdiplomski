package servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet("/http/file")
public class FileServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
   
    public FileServlet() {
        super();
    }

    
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String fileName=request.getParameter("fileName");
		if(fileName != null && !fileName.isEmpty() && request.getSession().getAttribute("username") != null) {
			String pathToFile="/WEB-INF/htmlFiles/"+request.getSession().getAttribute("username")+"/"+fileName+".html";
			System.out.println(pathToFile);
			request.getRequestDispatcher(pathToFile).forward(request, response);
		}
		else response.sendRedirect("forbidden.html");
	}

}
