<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">

<!-- Bootstrap CSS -->
<link rel="stylesheet" href="/resources/css/style.css">
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css"
	integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z"
	crossorigin="anonymous">

<title>Login</title>
</head>
<body>
		<div class="titleLogin">
			<p>HTTP Sharing System </p>
		</div>
		<div class="loginPart" id="loginPart">
		<form action="/http/login" method="POST">
			<div class="form-group">
				<label for="email">Username</label> <input type="text"
					placeholder="Enter Username" class="form-control"
					id="username" name="username" required>
			</div>
			<div class="form-group">
				<label for="password">Password</label> <input type="password"
					placeholder="Enter Password" class="form-control" id="password" name="password" required
					>
			</div>
			<div class="form-group" id="resultLogin">
				<h4><%= session.getAttribute("loginResult") != null ? session.getAttribute("loginResult"):"" %></h4>
			</div>
			<div class="form-group"></div>
			<div class="middleItem">
				<input type="submit" id="loginButton"  class="btn btn-success"  value="Login">
			</div>
			</form>
		</div>

	<!-- Optional JavaScript -->
	<!-- jQuery first, then Popper.js, then Bootstrap JS -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
	<script
		src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
	<script
		src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
</body>
</html>